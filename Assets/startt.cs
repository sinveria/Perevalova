﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class startt : MonoBehaviour
{
    public List<Sprite> sprites;
    private List<float> speeds = new List<float> { 2.0f, 1.0f, 0.5f };
    public GameObject auto;
    // Start is called before the first frame update
    void Start()
    {
        GameObject go = this.transform.GetChild(1).gameObject;
        go.SetActive(true);
        go = this.transform.GetChild(2).gameObject;
        Debug.Log(go.name);
        go.GetComponent<Animator>().enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void AutoStart(int number)
    {
        auto.GetComponent<Image>().sprite = sprites[number];
        GameObject go = this.transform.GetChild(2).gameObject;
        go.SetActive(true);
        Animator anim = go.GetComponent<Animator>();
        anim.speed = speeds[number];
        go = this.transform.GetChild(1).gameObject;
        go.SetActive(false);
        anim.enabled = true;
        auto.SetActive(true);
    }
}
